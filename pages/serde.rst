Serde
=====

All notes from https://serde.rs/

Serde is a framework for serializing and deserializing Rust data structures
efficiently and generically.
