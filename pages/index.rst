.. Learn Rust documentation master file, created by
   sphinx-quickstart on Thu May 30 09:48:19 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

An Overview Of Learning Rust & WebAssembly!
===========================================

Hosted on `Gitlab <https://gitlab.com/warfields/rust-notes>`_

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   rust_notes
   wasm
   rust_wasm
   wasm_contributing
   resources

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
