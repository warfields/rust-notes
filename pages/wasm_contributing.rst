Contributing to WebAssembly
===========================
It's quite complicated, I'm doing my best here to simplify the process.

The Community Group
-------------------
The CG is the governing body that determines WebAssembly's features and their
implementation. The group is free to join, with only one caveat. Your work
must be both open and free, thus your employer must not be able to claim your
work.

`Click here <https://www.w3.org/community/webassembly/>`_ for the site to join
the community group.

If it is remotely possible that you'll be working on a proposal then join the
CG and start attending the monthly meetings. I recomend offering to take notes
for the first couple of meetings. It's always needed, welcomed, and will force
you to getup to speed.

Proposals
---------
Every new proposal goes through five phases. `Exact details <https://github.com/WebAssembly/meetings/blob/master/process/phases.md#2-proposed-spec-text-available-community--working-group>`_

The process is long... If you want to be writing code the proposal must be at
least in phase two, when initial prototypes and test suites are made. Big
contributions are made on stage three when embedders implement the feature.

Stage two is the best place to hop onto if you want to experiment and help
implement on some project. See how `Google is doing this with multithreading <https://www.chromestatus.com/feature/5724132452859904>`_.
